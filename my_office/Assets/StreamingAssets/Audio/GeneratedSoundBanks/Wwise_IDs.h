/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AMB_GREENZONE = 1073787729U;
        static const AkUniqueID AMB_STREET = 1592801615U;
        static const AkUniqueID AQUARIUM = 456876952U;
        static const AkUniqueID FOOTSTEPS = 2385628198U;
        static const AkUniqueID GAME = 702482391U;
        static const AkUniqueID MUSIC_BIGROOM = 2555097960U;
        static const AkUniqueID PHONE = 3104856687U;
        static const AkUniqueID PLAY_ROOMTONE = 2748311587U;
        static const AkUniqueID PLAYROOM = 2131051366U;
        static const AkUniqueID TV = 1568083719U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace STATE_GAMESTATE
        {
            static const AkUniqueID GROUP = 1072505152U;

            namespace STATE
            {
                static const AkUniqueID BATTLE = 2937832959U;
                static const AkUniqueID MENU = 2607556080U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID PLANNING = 4030710154U;
            } // namespace STATE
        } // namespace STATE_GAMESTATE

        namespace STATE_INOUTDOORS
        {
            static const AkUniqueID GROUP = 1030318533U;

            namespace STATE
            {
                static const AkUniqueID GREENZONE = 813538312U;
                static const AkUniqueID HALL = 3633416828U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID PLAYROOM = 2131051366U;
                static const AkUniqueID STREET = 4142189312U;
            } // namespace STATE
        } // namespace STATE_INOUTDOORS

    } // namespace STATES

    namespace SWITCHES
    {
        namespace SWITCH_INT_GAMEPLAY_INTENCITY
        {
            static const AkUniqueID GROUP = 2617349748U;

            namespace SWITCH
            {
                static const AkUniqueID INT0 = 1405501216U;
                static const AkUniqueID INT1 = 1405501217U;
            } // namespace SWITCH
        } // namespace SWITCH_INT_GAMEPLAY_INTENCITY

        namespace SWITCH_SURFACE_TYPE
        {
            static const AkUniqueID GROUP = 2818941756U;

            namespace SWITCH
            {
                static const AkUniqueID SWITCH_SURFACE_CARPET = 117396797U;
                static const AkUniqueID SWITCH_SURFACE_CONCRETE = 3294452117U;
                static const AkUniqueID SWITCH_SURFACE_TILE = 2754391064U;
                static const AkUniqueID SWITCH_SURFACE_WOOD = 156175339U;
            } // namespace SWITCH
        } // namespace SWITCH_SURFACE_TYPE

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID EXT_CAMERA_HEIGHT = 1862344870U;
        static const AkUniqueID RTPC_OCCLUSION = 1664992182U;
        static const AkUniqueID RTPS_EXT_GAMEPLAY_INTENCITY = 2589518329U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MASTER = 4056684167U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBIENCE = 85412153U;
        static const AkUniqueID BIGROOM = 1495464960U;
        static const AkUniqueID CHARACTER = 436743010U;
        static const AkUniqueID GREENZONE = 813538312U;
        static const AkUniqueID HALL = 3633416828U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID OFFICE = 1547173777U;
        static const AkUniqueID OUTDOOR = 144697359U;
        static const AkUniqueID PLAYROOM = 2131051366U;
        static const AkUniqueID STREET = 4142189312U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID AUX_REVERB = 425396316U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
