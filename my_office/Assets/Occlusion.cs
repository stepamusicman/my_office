﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Occlusion : MonoBehaviour
{
    public AK.Wwise.Event bigroomevent;
    public GameObject Listener;
    public LayerMask lm;

    // Start is called before the first frame update
    void Start()
    {
        bigroomevent.Post(gameObject); 
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Physics.Linecast(gameObject.transform.position, Listener.transform.position, out hit, lm);
        if(hit.collider)
        {
            Debug.DrawLine(gameObject.transform.position, Listener.transform.position, Color.red);
            AkSoundEngine.SetRTPCValue("RTPC_occlusion", 1);
        }
        else
        {
            Debug.DrawLine(gameObject.transform.position, Listener.transform.position, Color.green);
            AkSoundEngine.SetRTPCValue("RTPC_occlusion", 0);
        }
    }
}
