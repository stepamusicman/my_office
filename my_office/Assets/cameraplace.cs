﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraplace : MonoBehaviour
{
    public GameObject Player;
    public GameObject Camera;
    public GameObject Listener;

    // Start is called before the first frame update
    void Start()
    {
       

    }
    // Update is called once per frame
    void Update()
    {

        Debug.DrawLine(Player.transform.position, Camera.transform.position);
        Listener.transform.position = ((Camera.transform.position - Player.transform.position) / 2) + Player.transform.position;
        Listener.transform.rotation = Camera.transform.rotation;

    }

}
